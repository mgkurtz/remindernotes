% Copyright 2022 Adrian Rettich, Markus Kurtz
%
% This file is part of the remindernotes LaTeX package.
% 
% This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
% 
% This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License along with this code. If not, see <https://www.gnu.org/licenses/>.

\ProvidesExplPackage {remindernotes} {2022/11/22} {0.2} {Margin Reminders}

\RequirePackage{pgfopts}
\RequirePackage{xparse} % is awesome, provides NewDocumentCommand

\RequirePackage{zref-abspage} % provides \theabspage
\RequirePackage[absolute]{textpos} % provides textblock* environment
% TODO I do not know, what \lastypos measures exactly, what position textblock*
% expects exactly and whether using textblock* is wise at all.

\newcommand\marginnotestyle{}

\dim_const:Nn \c_mn_width_dim { \marginparwidth }
\dim_const:Nn \c_mn_sep_dim { \baselineskip }
\cs_generate_variant:Nn \int_compare:nNnT { v }
\cs_generate_variant:Nn \dim_compare:nNnTF { v }
\cs_generate_variant:Nn \dim_max:nn { v }


% the \marginnote command takes one argument, namely the text for the note.
% It sets up the counters such that at shipout, that text is set in the margin
% on the same height as the \marginnote command was encountered.
% It takes as an optional argument a "family name" for the note.
% If multiple notes of the same family are encountered on the same
% page, only the one encountered first (while parsing!) is typeset.
% +m means mandatory argument that allows paragraphs inside.
\NewDocumentCommand \marginnote { O{} +m } {
  \tex_savepos:D
  \iow_shipout_x:Nn \@auxout {
    \exp_not:N \auxmarginnote
    { \exp_not:n {#2} }
    {#1}
    { \theabspage }
    { \int_use:N \tex_lastypos:D }
  }
}



% \auxmarginnote { text } { family } { page } { raw y pos }
% Save the marginnote for later processing
%   ⇔  `family` is empty
%    ∨  this is the first marginnote of family `family` on this page.
\DeclareRobustCommand \auxmarginnote [4] {
  \tl_if_empty:nTF {#2} {
    \mn_save:nnn {#1} {#3} {#4}
  } {
    \int_if_exist:cTF { g__mn_family_#2_last_seen_int } {
      \int_compare:vNnT { g__mn_family_#2_last_seen_int } < {#3}
        { \mn_save:nnn {#1} {#3} {#4} }
    } {
      \int_new:c { g__mn_family_#2_last_seen_int }
      \mn_save:nnn {#1} {#3} {#4}
    }
    \int_gset:cn { g__mn_family_#2_last_seen_int } {#3}
  }
}

% \mn_save:nnn { text } { page } { raw y pos }
% The raw y position is measured in scaled points (sp) from the bottom of the page.
% TODO To be honest, I have no idea, what it measures exactly.
\cs_new:Nn \mn_save:nnn {
  \seq_if_exist:cF { g__mn_notes_for_page_#2_seq } { \seq_new:c { g__mn_notes_for_page_#2_seq } }
  \seq_gput_right:cx { g__mn_notes_for_page_#2_seq } {
    { \exp_not:n {#1} }
    { \dim_eval:n { \pageheight + 42pt - #3sp } }
  }
}

% run our shipout routine on every page.
\AddToHook { shipout / before }{ \mn_shipout:n \theabspage }

% \mn_shipout:n { page }
\cs_new:Nn \mn_shipout:n {
  \seq_if_exist:cT { g__mn_notes_for_page_#1_seq } {
    \exp_args:Nc \__mn_shipout:N { g__mn_notes_for_page_#1_seq }
  }
}

% \__mn_shipout:N { sequence } where
% the sequence’s items are { { text } { y position } } where
% the y position is the distance to the page’s top.
%
% Create all boxes, resolve overlaps and finally use them.
\cs_new:Nn \__mn_shipout:N {
  \seq_sort:Nn #1 { % Make y pos of former item ≤ y pos of later item
    \dim_compare:nNnTF { \use_ii:nn ##1 } > { \use_ii:nn ##2 }
      \sort_return_swapped:
      \sort_return_same:
  }
  \seq_map_indexed_inline:Nn #1 { % ##1 = index, ##2 = { text } { y pos }
    \__mn_create_box:cn { l__mn_##1_box } { \use_i:nn ##2 }
    \__mn_dim_set_anew:cn { l__mn_ypos_##1_dim } { \use_ii:nn ##2 }
  }
  \int_set:Nn \__mn_box_count_int { \seq_count:N #1 }
  \mn_resolve_overlap:
  \int_step_function:nN \__mn_box_count_int \__mn_use_box:n
}

\cs_new:Npn \__mn_dim_set_anew:Nn #1 {
  \dim_if_exist:NF #1 { \dim_new:N #1 }
  \dim_set:Nn #1
}
\cs_generate_variant:Nn \__mn_dim_set_anew:Nn { c }


\cs_new:Nn \__mn_create_box:Nn { % { box name } { content }
  \box_clear_new:N #1
  \hbox_set:Nn #1 {
    \parbox [ c ] { \c_mn_width_dim } { \marginnotestyle \strut #2 }
  }
}
\cs_generate_variant:Nn \__mn_create_box:Nn { c }

\cs_new:Nn \__mn_use_box:n {
  \begin{textblock*} { \c_mn_width_dim } ( \c_mn_xpos_dim , \dim_use:c { l__mn_ypos_#1_dim } )
    \box_use:c { l__mn_#1_box }
  \end{textblock*}
}
\dim_const:Nn \c_mn_xpos_dim { \oddsidemargin + 1in + \hoffset + \textwidth + \marginparsep }

\int_new:N \__mn_box_count_int

% \mn_resolve_overlap:
% Solve the overlap resolution problem.
%% Assumptions
% א) Let the boxes be numbered 1 to 𝑁 in increasing initial y position.
%% Notation
% • Initial y position = y position saved in the sequence.
% • yₙ the y position of 𝑛.
% • shift(n) = yₙ − initial y position
% • Boxes 𝑛 and 𝑛+1 y-overlap iff there is less than \c_mn_sep_dim space
%   between them (potentially even negative space). Depends on yₙ and yₙ₊₁.
% • We say 𝑛 is y-good iff box 𝑛 and box 𝑛+1 do not y-overlap.
%% Implicit In- and Output
% in) 𝑁 = \__mn_box_count_int
% in&out) yₙ = \l__mn_ypos_𝑛_dim
% in) The heights of the boxes and \c_mn_sep_dim used in subroutines.
%% Constraints
% α) ∀ 1≤𝑛<𝑁: 𝑛 is y-good.  {Please note that we exclude the last box here.}
% β) ∀ 1≤𝑛≤𝑁: box 𝑛 must be above the page’s bottom. {Depends on yₙ and the bottom.}
%%% Simplified Constraints
% β’ ⇔ β {using α}) box 𝑁 must be above the page’s bottom.
% β’’ ⇔ β’ {using appropriately set y_{𝑁+1}}) 𝑁 is y-good.
% α’ ⇔ α∧β’’) ∀ 1≤𝑛≤𝑁: 𝑛 is y-good.
%% Optimization Criteria
% ν = ∑_{𝑛, shift(𝑛) < 0} -shift(n).  Sum of upward shifts.
% ω = ∑_𝑛 |shift(𝑛)|.                 Sum of all shifts.
%% Claims {Make sure you understand those, before proceeding to the implementation.}
% There is only one efficient solution, for which both ν and ω are minimal simultaneously.
% If the boxes need more vertical space than there is, they will spill over at the page’s top. Doomed.
\cs_new:Nn \mn_resolve_overlap: {
  %% ASSERT ν = ω = 0.

  %% Enforce (α): Shift y-bad boxes downward as much as needed
  % Thus increase ω only as much as needed. ν remains 0.
  % loop invariant ≔ ∀ 2≤𝑛<##1: 𝑛−1 is y-good.
  \int_step_inline:nnn 2 \__mn_box_count_int {
    \dim_set:cn { l__mn_ypos_##1_dim } { \dim_max:vn
      { l__mn_ypos_##1_dim }
      { \__mn_ypos_below:n { \int_eval:n { ##1 - 1 } } }
    }
  }

  %% Set y_{𝑁+1} fittingly.
  \__mn_dim_set_anew:cn { l__mn_ypos_ \int_eval:n { \__mn_box_count_int + 1 } _dim } { \pageheight }

  %% ASSERT (α) holds with minimal ω. ν=0.

  %% Enforce (α’): Shift y-bad boxes upward as much as needed
  % Thus keep ω constant, and increase ν only as much as needed.
  % loop invariant ≔ ∀ 1≤𝑛≤N, n≠\l_tmpa_int: 𝑛 is y-good.
  \int_set_eq:NN \l_tmpa_int \__mn_box_count_int
  \int_while_do:nNnn \l_tmpa_int > 0 {
    \dim_set:Nn \l_tmpa_dim { \__mn_ypos_above_other:nn { \l_tmpa_int } { \l_tmpa_int + 1 } }
    \dim_compare:vNnTF { l__mn_ypos_ \int_use:N \l_tmpa_int _dim } > \l_tmpa_dim {
      % \l_tmpa_int is bad, move its box upward to make it good, possibly making $\l_tmpa_int - 1$ bad.
      \iow_term:x { Bump~up~box~\int_use:N \l_tmpa_int \space on~page~\theabspage. }
      \dim_set_eq:cN { l__mn_ypos_ \int_use:N \l_tmpa_int _dim } \l_tmpa_dim
      \int_decr:N \l_tmpa_int
    } {
      % loop invariant ∧ \l_tmpa_int is good ⇔ (α’). Everything is good, so we are free to break the loop.
      \int_set:Nn \l_tmpa_int 0
    }
  }

  %% ASSERT (α’) holds with minimal ν and minimal ω. Optimal solution found.
}

\cs_new:Nn \__mn_ypos_below:n { % { index of box }
  \dim_use:c { l__mn_ypos_#1_dim }
  + \box_ht_plus_dp:c { l__mn_#1_box }
  + \c_mn_sep_dim
}

% \__mn_ypos_above_other:nn { index of box to position } { index of other box }
% y position for which box № #1 would lie just on top of box № #2.
\cs_new:Nn \__mn_ypos_above_other:nn {
  \dim_use:c { l__mn_ypos_ \int_eval:n {#2} _dim }
  - \box_ht_plus_dp:c { l__mn_ \int_eval:n {#1} _box }
  - \c_mn_sep_dim
}

%%% Reminders

% \NewReminder \cs { argument specification } { content } [ family ] { reminder } [ reference ]
\NewDocumentCommand \NewReminder { m m m O{ \cs_to_str:N #1 } m O{ \mn_label:Nn #1 {#4} } } {
  \exp_args:Nxxx \__mn_new_reminder:nnnNnnn
    {#4}
    { \tl_if_empty:nF {#6} { \exp_not:N \label {#6} } }
    { \tl_if_empty:nF {#6} { ~ \exp_not:N \ref {#6} } }
    {#1} {#2} {#3} {#5}
}

% #1=family, #2=label, #3=ref, #4=\cs, #5=args, #6=content, #7=reminder
\cs_new:Nn \__mn_new_reminder:nnnNnnn {
  \NewDocumentCommand {#4} {s #5} {
    #6
    \seq_if_in:NnF \g_mn_suppressed_on_current_page_seq {#1} {
      \bool_if:NTF ##1
        { \mn_suppress_reminder:n {#1} #2 }
        { \marginnote [#1] { #7 #3 } }
    }
  }
}

\cs_set:Nn \mn_label:Nn { rem : #2 }

\cs_new:Npn \mn_suppress_reminder:n { \seq_gpush:Nn \g_mn_suppressed_on_current_page_seq }
\cs_set_eq:NN \SuppressReminder \mn_suppress_reminder:n

\seq_new:N \g_mn_suppressed_on_current_page_seq
\AddToHook { shipout / after } { \seq_gclear:N \g_mn_suppressed_on_current_page_seq }

%%% === %%%

% The following commands provide backwards compatibility with Adrian's thesis.
% They are NOT intended for the final package.

\newcommand \reminder [5] {
  \exp_args:Ncx \NewReminder {#1} { s \prg_replicate:nn {#2} m } { \ensuremath {#3} } {#4} [#5]
}

\let\Marginfill\marginnote
